import { Test, TestingModule } from '@nestjs/testing';
import { GettingAmountService } from './get-amount.service';
import { ValuesFeePayment } from '../../shared/domain/fee-payment';
import { TitlesAvailables } from '../../shared/domain/id-titles';
import { ValuesAvailables } from '../../shared/domain/value-titles';
import { InitTitleBankDto } from '../../bankTitle/application/dtos/init-bank-title.dto';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';

describe('Get amount titles Service', () => {
  let gettingAmountService: GettingAmountService;
  let bdFileStorageService;
  const mockBdModel = () => ({
    openFile: jest.fn(),
    saveFile: jest.fn(),
  });
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        GettingAmountService,
        { provide: BDFileStorageService, useFactory: mockBdModel },
      ],
    }).compile();
    bdFileStorageService =
      module.get<BDFileStorageService>(BDFileStorageService);
    gettingAmountService =
      module.get<GettingAmountService>(GettingAmountService);
  });

  it('should be defined', () => {
    expect(gettingAmountService).toBeDefined();
  });
  it('Create title', async () => {
    const tittleDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
    ];

    bdFileStorageService.openFile.mockResolvedValue(tittleDto);

    const result = await gettingAmountService.run();

    expect(result).toEqual({ count: 1 });
  });
});
