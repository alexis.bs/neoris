import { IsNotEmpty, IsEnum } from 'class-validator';
import { TitlesAvailables } from '../../../shared/domain/id-titles';
import { ValuesAvailables } from '../../../shared/domain/value-titles';
import { ValuesFeePayment } from '../../../shared/domain/fee-payment';

const valuesFeePayment = [ValuesFeePayment.n, ValuesFeePayment.y];
const valuesAvailables = [
  ValuesAvailables.tesi,
  ValuesAvailables.tesoros,
  ValuesAvailables.tesp,
  ValuesAvailables.test,
  ValuesAvailables.tesu,
  ValuesAvailables.thi,
  ValuesAvailables.tid,
  ValuesAvailables.tp,
  ValuesAvailables.trpv,
  ValuesAvailables.usd,
];
const titlesAvailables = [
  TitlesAvailables.tesi,
  TitlesAvailables.tesoros,
  TitlesAvailables.tesp,
  TitlesAvailables.test,
  TitlesAvailables.tesu,
  TitlesAvailables.thi,
  TitlesAvailables.tid,
  TitlesAvailables.tp,
  TitlesAvailables.trpv,
  TitlesAvailables.usd,
];
export class InitTitleBankDto {
  @IsNotEmpty()
  @IsEnum(titlesAvailables)
  idTittle: TitlesAvailables;

  @IsNotEmpty()
  @IsEnum(valuesAvailables)
  tittle: ValuesAvailables;

  @IsNotEmpty()
  classification: string;

  @IsNotEmpty()
  value: number;

  @IsNotEmpty()
  createdAt: Date;

  @IsNotEmpty()
  expiredDate: Date;

  @IsNotEmpty()
  @IsEnum(valuesFeePayment)
  feePayment: ValuesFeePayment;
}
