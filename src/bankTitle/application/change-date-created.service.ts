import { Injectable, Logger } from '@nestjs/common';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';
import { bankTitle } from '../domain/entities/bank-title';

@Injectable()
export class UpdateDateService {
  private readonly logger = new Logger(UpdateDateService.name);
  constructor(private bdFileStorageService: BDFileStorageService) {}

  async run(): Promise<bankTitle[]> {
    const data = await this.bdFileStorageService.openFile();
    const date = new Date('2022-01-25');

    const titles = data.map((element: bankTitle) => {
      if (date.getTime() === new Date(element.createdAt).getTime()) {
        element.createdAt = new Date('2022-02-01');
      }
      return element;
    });

    await this.bdFileStorageService.saveFile(titles);

    this.logger.log(`Success dates updates`);
    return titles;
  }
}
