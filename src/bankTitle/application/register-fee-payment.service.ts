import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';
import { bankTitle } from '../domain/entities/bank-title';
import { ValuesFeePayment } from '../../shared/domain/fee-payment';
import { TitlesAvailables } from '../../shared/domain/id-titles';

@Injectable()
export class SetPaymentService {
  private readonly logger = new Logger(SetPaymentService.name);
  constructor(private bdFileStorageService: BDFileStorageService) {}

  async run(id: TitlesAvailables): Promise<bankTitle> {
    let data = await this.bdFileStorageService.openFile();

    const title: bankTitle = data.find((element: bankTitle) => {
      if (id.toLocaleLowerCase() === element.idTittle.toLocaleLowerCase()) {
        return element;
      }
    });

    if (!title) {
      throw new BadRequestException('element_not_found');
    }

    if (title.feePayment === ValuesFeePayment.y) {
      throw new BadRequestException('payment_already_approved');
    }

    title.feePayment = ValuesFeePayment.y;

    data = data.map((element: bankTitle) => {
      if (id.toLocaleLowerCase() === element.idTittle.toLocaleLowerCase()) {
        return title;
      }
      return element;
    });

    const response = await this.bdFileStorageService.saveFile(data);

    this.logger.log(`Success payment registered `);
    return response;
  }
}
