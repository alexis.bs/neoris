import { Injectable, Logger } from '@nestjs/common';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';

@Injectable()
export class GettingAmountService {
  private readonly logger = new Logger(GettingAmountService.name);
  constructor(private bdFileStorageService: BDFileStorageService) {}

  async run(): Promise<{ count: number }> {
    const data = await this.bdFileStorageService.openFile();
    const response = {
      count: Number(data.length),
    };
    this.logger.log(`Success get count: ${response.count}`);
    return response;
  }
}
