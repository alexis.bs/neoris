import { Test, TestingModule } from '@nestjs/testing';
import { SetPaymentService } from './register-fee-payment.service';
import { ValuesFeePayment } from '../../shared/domain/fee-payment';
import { TitlesAvailables } from '../../shared/domain/id-titles';
import { ValuesAvailables } from '../../shared/domain/value-titles';
import { InitTitleBankDto } from './dtos/init-bank-title.dto';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';
import { HttpStatus } from '@nestjs/common';

describe('Register fee payment Service', () => {
  let setPaymentService: SetPaymentService;
  let bdFileStorageService;
  const mockBdModel = () => ({
    openFile: jest.fn(),
    saveFile: jest.fn(),
  });
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        SetPaymentService,
        { provide: BDFileStorageService, useFactory: mockBdModel },
      ],
    }).compile();
    bdFileStorageService =
      module.get<BDFileStorageService>(BDFileStorageService);
    setPaymentService = module.get<SetPaymentService>(SetPaymentService);
  });

  it('should be defined', () => {
    expect(setPaymentService).toBeDefined();
  });
  it('Register payment title', async () => {
    const tittleDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
    ];

    const tittleResponseDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.y,
      },
    ];
    const spy = jest.spyOn(bdFileStorageService, 'saveFile');
    bdFileStorageService.openFile.mockResolvedValue(tittleDto);
    bdFileStorageService.saveFile.mockResolvedValue(tittleDto);
    const result = await setPaymentService.run(TitlesAvailables.usd);
    expect(spy).toHaveBeenCalled();
    expect(result).toEqual(tittleResponseDto);
  });

  it('Title not found', async () => {
    const tittleDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
    ];

    bdFileStorageService.openFile.mockResolvedValue(tittleDto);
    try {
      await setPaymentService.run(TitlesAvailables.tesi);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.BAD_REQUEST);
    }
  });

  it('Title payment already approved', async () => {
    const tittleDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.y,
      },
    ];

    bdFileStorageService.openFile.mockResolvedValue(tittleDto);
    try {
      await setPaymentService.run(TitlesAvailables.usd);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.BAD_REQUEST);
    }
  });
});
