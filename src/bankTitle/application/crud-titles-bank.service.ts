import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';
import { bankTitle } from '../domain/entities/bank-title';
import { TitlesAvailables } from '../../shared/domain/id-titles';
import { ValuesAvailables } from '../../shared/domain/value-titles';

@Injectable()
export class CrudTittleService {
  private readonly logger = new Logger(CrudTittleService.name);
  constructor(private bdFileStorageService: BDFileStorageService) {}

  async delete(id: TitlesAvailables): Promise<bankTitle[]> {
    const data = await this.bdFileStorageService.openFile();

    const titles = data.filter((element: bankTitle) => {
      if (id.toLocaleLowerCase() !== element.idTittle.toLocaleLowerCase()) {
        return element;
      }
    });

    await this.bdFileStorageService.saveFile(titles);
    this.logger.log(`Success title deleted `);
    return titles;
  }

  async getAll(): Promise<bankTitle[]> {
    const data = await this.bdFileStorageService.openFile();

    this.logger.log(`Success get all tittles `);
    return data;
  }

  async create(bankTitle: bankTitle): Promise<bankTitle[]> {
    const data = await this.bdFileStorageService.openFile();

    const title: bankTitle = data.find((element: bankTitle) => {
      if (
        bankTitle.idTittle.toLocaleLowerCase() ===
        element.idTittle.toLocaleLowerCase()
      ) {
        return element;
      }
    });
    const aux = Object.keys(ValuesAvailables).find((key) => {
      if (bankTitle.tittle === ValuesAvailables[key]) {
        return key;
      }
    });

    if (bankTitle.idTittle !== TitlesAvailables[aux]) {
      throw new BadRequestException('id_title is diferente to value of title');
    }
    if (title) {
      this.logger.log(`title existed `);
      throw new BadRequestException('element_existed');
    }

    data.push(bankTitle);

    await this.bdFileStorageService.saveFile(data);

    this.logger.log(`Success title created `);
    return data;
  }
}
