import { Test, TestingModule } from '@nestjs/testing';
import { UpdateDateService } from './change-date-created.service';
import { ValuesFeePayment } from '../../shared/domain/fee-payment';
import { TitlesAvailables } from '../../shared/domain/id-titles';
import { ValuesAvailables } from '../../shared/domain/value-titles';
import { InitTitleBankDto } from './dtos/init-bank-title.dto';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';
import { HttpStatus } from '@nestjs/common';

describe('Register fee payment Service', () => {
  let updateDateService: UpdateDateService;
  let bdFileStorageService;
  const mockBdModel = () => ({
    openFile: jest.fn(),
    saveFile: jest.fn(),
  });
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        UpdateDateService,
        { provide: BDFileStorageService, useFactory: mockBdModel },
      ],
    }).compile();
    bdFileStorageService =
      module.get<BDFileStorageService>(BDFileStorageService);
    updateDateService = module.get<UpdateDateService>(UpdateDateService);
  });

  it('should be defined', () => {
    expect(UpdateDateService).toBeDefined();
  });
  it('update date title', async () => {
    const tittleDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
      {
        idTittle: TitlesAvailables.tesi,
        tittle: ValuesAvailables.tesi,
        classification: 'DIV',
        value: 4000000,
        createdAt: new Date('2022-01-25'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
      {
        idTittle: TitlesAvailables.tesoros,
        tittle: ValuesAvailables.tesoros,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-05-14'),
        expiredDate: new Date('2023-06-17'),
        feePayment: ValuesFeePayment.n,
      },
    ];

    const tittleResponseDto: InitTitleBankDto[] = [
      {
        idTittle: TitlesAvailables.usd,
        tittle: ValuesAvailables.usd,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-03-14'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
      {
        idTittle: TitlesAvailables.tesi,
        tittle: ValuesAvailables.tesi,
        classification: 'DIV',
        value: 4000000,
        createdAt: new Date('2022-02-01'),
        expiredDate: new Date('2023-03-15'),
        feePayment: ValuesFeePayment.n,
      },
      {
        idTittle: TitlesAvailables.tesoros,
        tittle: ValuesAvailables.tesoros,
        classification: 'DIV',
        value: 500000000,
        createdAt: new Date('2022-05-14'),
        expiredDate: new Date('2023-06-17'),
        feePayment: ValuesFeePayment.n,
      },
    ];
    const spy = jest.spyOn(bdFileStorageService, 'saveFile');
    bdFileStorageService.openFile.mockResolvedValue(tittleDto);
    bdFileStorageService.saveFile.mockResolvedValue(tittleResponseDto);
    const result = await updateDateService.run();
    expect(spy).toHaveBeenCalled();
    expect(result).toEqual(tittleResponseDto);
  });
});
