import { Test, TestingModule } from '@nestjs/testing';
import { CrudTittleService } from './crud-titles-bank.service';
import { ValuesFeePayment } from '../../shared/domain/fee-payment';
import { TitlesAvailables } from '../../shared/domain/id-titles';
import { ValuesAvailables } from '../../shared/domain/value-titles';
import { InitTitleBankDto } from './dtos/init-bank-title.dto';
import { BDFileStorageService } from '../../shared/infrastructure/bd.service';
import { HttpStatus } from '@nestjs/common';

describe('Basic crud service', () => {
  let crudTittleService: CrudTittleService;
  let bdFileStorageService;
  const mockBdModel = () => ({
    openFile: jest.fn(),
    saveFile: jest.fn(),
  });
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        CrudTittleService,
        { provide: BDFileStorageService, useFactory: mockBdModel },
      ],
    }).compile();
    bdFileStorageService =
      module.get<BDFileStorageService>(BDFileStorageService);
    crudTittleService = module.get<CrudTittleService>(CrudTittleService);
  });
  it('should be defined', () => {
    expect(crudTittleService).toBeDefined();
  });

  describe('Get all', () => {
    it('Get all tittles ', async () => {
      const tittleDto: InitTitleBankDto[] = [
        {
          idTittle: TitlesAvailables.usd,
          tittle: ValuesAvailables.usd,
          classification: 'DIV',
          value: 500000000,
          createdAt: new Date('2022-03-14'),
          expiredDate: new Date('2023-03-15'),
          feePayment: ValuesFeePayment.n,
        },
        {
          idTittle: TitlesAvailables.tesi,
          tittle: ValuesAvailables.tesi,
          classification: 'DIV',
          value: 350000000,
          createdAt: new Date('2022-02-16'),
          expiredDate: new Date('2023-09-15'),
          feePayment: ValuesFeePayment.y,
        },
      ];

      bdFileStorageService.openFile.mockResolvedValue(tittleDto);

      const result = await crudTittleService.getAll();
      expect(result).toEqual(tittleDto);
    });
  });

  describe('Delete', () => {
    it('Delete one tittle ', async () => {
      const tittleDto: InitTitleBankDto[] = [
        {
          idTittle: TitlesAvailables.usd,
          tittle: ValuesAvailables.usd,
          classification: 'DIV',
          value: 500000000,
          createdAt: new Date('2022-03-14'),
          expiredDate: new Date('2023-03-15'),
          feePayment: ValuesFeePayment.n,
        },
        {
          idTittle: TitlesAvailables.tesi,
          tittle: ValuesAvailables.tesi,
          classification: 'DIV',
          value: 350000000,
          createdAt: new Date('2022-02-16'),
          expiredDate: new Date('2023-09-15'),
          feePayment: ValuesFeePayment.y,
        },
      ];
      const tittleResponseDto: InitTitleBankDto[] = [
        {
          idTittle: TitlesAvailables.tesi,
          tittle: ValuesAvailables.tesi,
          classification: 'DIV',
          value: 350000000,
          createdAt: new Date('2022-02-16'),
          expiredDate: new Date('2023-09-15'),
          feePayment: ValuesFeePayment.y,
        },
      ];

      const spy = jest.spyOn(bdFileStorageService, 'saveFile');
      bdFileStorageService.openFile.mockResolvedValue(tittleDto);
      bdFileStorageService.saveFile.mockResolvedValue(tittleResponseDto);
      const result = await crudTittleService.delete(TitlesAvailables.usd);
      expect(spy).toHaveBeenCalled();
      expect(result).toEqual(tittleResponseDto);
    });
  });

  describe('Create', () => {
    it('Create one tittle ', async () => {});
  });
});
