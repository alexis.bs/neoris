import { TitlesAvailables } from '../../../shared/domain/id-titles';
import { ValuesAvailables } from '../../../shared/domain/value-titles';
import { ValuesFeePayment } from '../../../shared/domain/fee-payment';

export interface bankTitle {
  idTittle: TitlesAvailables;
  tittle: ValuesAvailables;
  classification: string;
  value: number;
  createdAt: Date;
  expiredDate: Date;
  feePayment: ValuesFeePayment;
}
