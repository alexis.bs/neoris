import {
  BadRequestException,
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  PreconditionFailedException,
  Put,
  Delete,
  Req,
} from '@nestjs/common';

import { GettingAmountService } from '../../application/get-amount.service';
import { SetPaymentService } from '../../application/register-fee-payment.service';
import { TitlesAvailables } from '../../../shared/domain/id-titles';
import { UpdateDateService } from '../../application/change-date-created.service';
import { CrudTittleService } from '../../application/crud-titles-bank.service';
import { InitTitleBankDto } from '../../application/dtos/init-bank-title.dto';

@Controller('titles')
export class TitlesBankController {
  constructor(
    private gettingAmount: GettingAmountService,
    private setPaymentService: SetPaymentService,
    private updateDateService: UpdateDateService,
    private crudTittleService: CrudTittleService,
  ) {}

  @Get('amount')
  getAmount() {
    return this.gettingAmount.run();
  }

  @Put('register/payment/:id')
  setPayment(@Param('id') id: TitlesAvailables) {
    return this.setPaymentService.run(id);
  }

  @Put('update/date')
  updateDate() {
    return this.updateDateService.run();
  }

  @Delete('delete/:id')
  deleteTittle(@Param('id') id: TitlesAvailables) {
    return this.crudTittleService.delete(id);
  }

  @Get('getall')
  getAll() {
    return this.crudTittleService.getAll();
  }

  @Post('create')
  receiveDocuments(@Body() body: InitTitleBankDto) {
    return this.crudTittleService.create(body);
  }
}
