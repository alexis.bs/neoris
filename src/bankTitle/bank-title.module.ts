import { Module } from '@nestjs/common';
import { TitlesBankController } from './infrastructure/controllers/bank-title.controller';
import { GettingAmountService } from './application/get-amount.service';
import { UpdateDateService } from './application/change-date-created.service';
import { SetPaymentService } from './application/register-fee-payment.service';
import { CrudTittleService } from './application/crud-titles-bank.service';
import { BDFileStorageService } from '../shared/infrastructure/bd.service';

@Module({
  imports: [],
  controllers: [TitlesBankController],
  providers: [
    GettingAmountService,
    BDFileStorageService,
    SetPaymentService,
    UpdateDateService,
    CrudTittleService,
  ],
})
export class BankTitleModule {}
