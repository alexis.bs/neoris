import { Module } from '@nestjs/common';
import { SharedModule } from './shared/shared.module';
import { BankTitleModule } from './bankTitle/bank-title.module';
import { ENVS } from './shared/domain/config/schema-env-variables';
import { LoggerModule } from 'nestjs-pino';
@Module({
  imports: [
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
        transport:
          process.env.NODE_ENV === ENVS.LOCAL
            ? { target: 'pino-pretty' }
            : undefined,
        serializers: {
          req(req) {
            req.body = req.raw.body;
            return req;
          },
        },
      },
    }),
    BankTitleModule,
    SharedModule,
  ],
})
export class AppModule {}
