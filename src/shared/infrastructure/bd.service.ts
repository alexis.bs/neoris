import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { readFile, writeFile } from 'fs/promises';

@Injectable()
export class BDFileStorageService {
  private readonly logger = new Logger(BDFileStorageService.name);
  private filePath: string;
  constructor() {
    this.filePath = process.cwd() + '/bd.json';
  }

  async openFile() {
    let data = null;
    try {
      data = await readFile(this.filePath);
      data = JSON.parse(data);
    } catch (error) {
      this.logger.error(` error open file , error: ${error} `);
      throw new BadRequestException('error_open_to_file');
    }

    return data;
  }

  async saveFile(data) {
    try {
      data = await writeFile(this.filePath, JSON.stringify(data));
    } catch (error) {
      this.logger.error(` error save file , error: ${error} `);
      throw new BadRequestException('error_save_to_file');
    }

    return data;
  }
}
