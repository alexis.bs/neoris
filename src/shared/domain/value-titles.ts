export enum ValuesAvailables {
  usd = 'DOLAR',
  trpv = 'TITULO DE PARTICIPACION RENTA VARIABLE',
  tp = 'TITULO DE PARTICIPACION',
  tid = 'TIDIS',
  thi = 'TITULOS HIPOTECARIOS',
  tesu = 'TES UVR',
  test = 'TES TRM',
  tesp = 'TES PESOS',
  tesoros = 'BONOS DEL TESORO EEUU',
  tesi = 'TES IPC',
}
