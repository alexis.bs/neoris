export enum TitlesAvailables {
  usd = 'USD',
  trpv = 'TRPV',
  tp = 'TP',
  tid = 'TID',
  thi = 'THI',
  tesu = 'TESU',
  test = 'TEST',
  tesp = 'TESP',
  tesoros = 'TESOROS',
  tesi = 'TESI',
}
