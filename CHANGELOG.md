# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/alexis.bs/neoris/v0.1.0...v0.2.0) (2023-01-24)


### Features

* add entity for bank title ([bc6e22e](https://gitlab.com/alexis.bs/neoris/commit/bc6e22e3a9fae6345f3f30d2c70ec10e008936fb))
* add register fee service and test ([21a5fab](https://gitlab.com/alexis.bs/neoris/commit/21a5fabcede40dc11127b0322110ccd3b7d8478d))
* added module for bank title and controller ([a66ff5c](https://gitlab.com/alexis.bs/neoris/commit/a66ff5c878aaf03a78c804faa28025e27a47d826))
* amount service and test ([b19afd7](https://gitlab.com/alexis.bs/neoris/commit/b19afd71cba885c015a8c9e61a1f566831ce4809))
* change data service and test ([20de04d](https://gitlab.com/alexis.bs/neoris/commit/20de04df6e8a23118a49db77b069fd9a4ba4e835))
* crud tittles bank service ([5357b07](https://gitlab.com/alexis.bs/neoris/commit/5357b075d9cee753d4af9a2ad3c36d4d67cda9fc))
* enums for features of bank titles ([b445e09](https://gitlab.com/alexis.bs/neoris/commit/b445e09b4f125780dcbd5dd5b20ca2f8e7432c5a))


### Chore

* update last configuration ([c2bea4d](https://gitlab.com/alexis.bs/neoris/commit/c2bea4dd1e5223efb167036fbfcaca265d91cb05))

## 0.1.0 (2023-01-19)


### Features

* add env variables ([218a265](https://gitlab.com/alexis.bs/neoris/commit/218a2650f3b9d9ad8e2958fb9e69d6c6bfc56477))
* add module shared ([f1cc407](https://gitlab.com/alexis.bs/neoris/commit/f1cc407241b3a658533b1ca441a96ac5de228de0))
* add new modules ([cd548a4](https://gitlab.com/alexis.bs/neoris/commit/cd548a4d99c7019ad0f21bc121be2499dfb21594))
* added conventianol messages for commits with husky ([0052f56](https://gitlab.com/alexis.bs/neoris/commit/0052f56a50f5cd1e3f493654f0abc970a9a48932))
* added nvmrc file ([1b3bbc3](https://gitlab.com/alexis.bs/neoris/commit/1b3bbc3be1cdabfb32e11be70acf7e3dbeb24cff))
* added others files of configuration ([2a83ff1](https://gitlab.com/alexis.bs/neoris/commit/2a83ff1a15fb0057131a0a9c45f7db6fc1c6fb8e))


### Chore

* change format of main module ([7b1cae3](https://gitlab.com/alexis.bs/neoris/commit/7b1cae355672b14e737f96e792cc9a2569dda36c))
* change format of main module  ([b18f6a1](https://gitlab.com/alexis.bs/neoris/commit/b18f6a1400aa8649234259a8574945b9a6f9f318))
